import React from 'react';
import { storiesOf } from '@storybook/react';
import { WithNotes } from '@storybook/addon-notes';
import { withInfo } from '@storybook/addon-info';
import { withKnobs } from '@storybook/addon-knobs';
import WithViewport from '../../../HOC/WithViewport';
import icons from 'Style/icons/icons.svg';
import logoSVG from 'Style/icons/whatcar_logo.svg';

import HeaderWc from '../';
import themeWC from '../headerWc.scss';
import withTheme from '../../../HOC/WithTheme';
const HeaderWcWC = withTheme(HeaderWc, themeWC);

const props = {
  iconArrowDown: `${icons}#wci-arrow-down`,
  iconArrowUp: `${icons}#wci-arrow-up`,
  iconMobileMenu: `${icons}#wci-burger`,
  iconMyDeals: `${icons}#wci-car`,
  iconMobileMenuClose: `${icons}#wci-cross`,
  iconSearch: `${icons}#wci-search`,
  iconAvatar: `${icons}#wci-avatar`,
  logoSVG: logoSVG,
  style: themeWC
}

storiesOf('WhatCarSpecific/HeaderWc', module)
  .addDecorator(withKnobs)
  .add(
    'Info doc',
    withInfo({
      text: 'Component usage'
    })(() => <HeaderWc viewport={'lg'} {...props} />)
  )
  .add('Live Preview WC Logged out', () => {
    document.cookie = 'userInfo=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
     
    return (
      <WithNotes notes={'This is the HeaderWc component for when a buyer is logged Out'}>
        <WithViewport>
        {
          viewportSize => (
            <div>
            <HeaderWcWC
              newCbeUrl='newcarbuying.whatcar.com'
              viewport={ viewportSize }
              iconArrowDown={`${icons}#wci-arrow-down`}
              iconArrowUp={`${icons}#wci-arrow-up`}
              iconMobileMenu={`${icons}#wci-burger`}
              iconMyDeals={`${icons}#wci-car`}
              iconMobileMenuClose={`${icons}#wci-cross`}
              iconSearch={`${icons}#wci-search`}
              iconAvatar={`${icons}#wci-avatar`}
              logoSVG={logoSVG}
              style={themeWC}
            />
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ex alias magnam sunt reiciendis quos soluta laudantium, asperiores sed in recusandae et tempora maiores nemo aliquid ipsam eligendi cumque. Ratione, temporibus?</p>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ex alias magnam sunt reiciendis quos soluta laudantium, asperiores sed in recusandae et tempora maiores nemo aliquid ipsam eligendi cumque. Ratione, temporibus?</p>
            
            </div>
        )
        }
        </WithViewport>
    </WithNotes>
    );
  }
)
  .add('Live Preview WC Logged in', () => {
    document.cookie="userInfo=Zoidberg"
    
    return (
      <WithNotes notes={'This is the HeaderWc component for when a buyer is logged in'}>
        <WithViewport>
        {
          viewportSize => (
            <div>
            <HeaderWcWC
              viewport={ viewportSize }
              iconArrowDown={`${icons}#wci-arrow-down`}
              iconArrowUp={`${icons}#wci-arrow-up`}
              iconMobileMenu={`${icons}#wci-burger`}
              iconMyDeals={`${icons}#wci-car`}
              iconMobileMenuClose={`${icons}#wci-cross`}
              iconSearch={`${icons}#wci-search`}
              iconAvatar={`${icons}#wci-avatar`}
              logoSVG={logoSVG}
              style={themeWC}
            />
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ex alias magnam sunt reiciendis quos soluta laudantium, asperiores sed in recusandae et tempora maiores nemo aliquid ipsam eligendi cumque. Ratione, temporibus?</p>
            
            </div>
        )
        }
        </WithViewport>
    </WithNotes>
    );
  }
).add('Live Preview WC (External)', () => {  
    
    // Store the icons paths/dimensions in objects
    const svgDefault = {
      viewBox: '0 0 512 512'
    };

    const svgIcons = {
      arrowDown: {
        ...svgDefault,
        path: 'm483 114l-227 227-227-227-29 28 256 256 256-256z'
      },
      arrowUp: {
        ...svgDefault,
        path: 'm0 370l29 28 227-227 227 227 29-28-256-256z'
      },
      menuOpen: {
        ...svgDefault,
        path:
          'm1 99l512 0 0 39-512 0z m-1 138l512 0 0 39-512 0z m0 138l512 0 0 39-512 0z'
      },
      menuClose: {
        ...svgDefault,
        path:
          'm512 37l-37-37-219 219-219-219-37 37 219 219-219 219 37 37 219-216 219 216 37-37-219-219z'
      }
    };

    return (
      <WithNotes notes={'This is the HeaderWc component for when the header is being used externally'}>
        <WithViewport>
        {
          viewportSize => (
            <HeaderWcWC
              viewport={ viewportSize }
              iconArrowDown={svgIcons.arrowDown}
              iconArrowUp={svgIcons.arrowUp}
              iconMobileMenu={svgIcons.menuOpen}
              iconMobileMenuClose={svgIcons.menuClose}
              logoSVG={logoSVG}
              style={themeWC}
              isUsedExternal={true}
            />
          )
        }
        </WithViewport>
    </WithNotes>
    );
  }
);

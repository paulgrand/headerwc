import React from 'react';
import Button from 'Presentational/Button';
import MenuMobile from './';
import data from '../../data/leftMenu';
import HayNavLink from 'Presentational/HayNavLink';
import style from './menuMobile.scss';

const someUrl = 'www.hello.com';
const dataObj = data(someUrl);

const props = {
  data: dataObj,
  linkComponent: HayNavLink,
  iconMobileMenuClose: 'iconMobileMenuClose',
  iconMobileMenu: 'iconMobileMenu',
  iconArrowUp: 'iconArrowUp',
  iconArrowDown: 'iconArrowDown',
  iconMyDeals: 'iconMyDeals',
  userFullName: 'Michael Jordan',
  isUserLoggedIn: false,
  style: style
};

describe('<MenuMobile />', () => {
  
  const wrapper = shallow(<MenuMobile {...props} />);

  it('should render MenuMobile', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('should have a class clickable', () => {
    expect(wrapper.find('.classMenuMobileClickable')).toHaveLength(1);
  });

  describe('Icon IconMobileMenu', () => {
    const icon = wrapper.find('Icon');
    it('should have the Icon component', () => {
      expect(icon).toHaveLength(1);
    });
    it('should have the icon iconMobileMenu', () => {
      expect(icon.props().iconName).toEqual('iconMobileMenu');
    });
    it('should have size lg', () => {
      expect(icon.props().size).toEqual('lg');
    });
    it('should have color white', () => {
      expect(icon.props().color).toEqual('white');
    });
    it('should permit passing an icon prop as an object', () => {
      const wrapper = mount(<MenuMobile {...props} iconMobileMenu={{}}/>);
      const iconSvgProp = wrapper.find('Icon').findWhere(component => !!component.prop('iconSvgObject'));
      const renderedComponent = mount(iconSvgProp.get(0));
      const iconSvgObjectProp = renderedComponent.prop('iconSvgObject');
      expect(iconSvgObjectProp).toMatchObject({});
    });

    describe('When click on IconMobileMenu', () => {
      
      it('should NOT have a class classMenuMobileOpen when menu is close', () => {
        expect(wrapper.find('.classMenuMobileOpen')).toHaveLength(0);
      });

      it('should change state when click on IconMobileMenu', () => {
        expect(wrapper.state().isMenuOpen).toBeFalsy();
        expect(wrapper.find('.classMenuMobileClickable').simulate('click'));
        expect(wrapper.state().isMenuOpen).toBeTruthy();
      });

      it('should have a class menuMobileOpen when menu is open', () => {
        expect(wrapper.find('.classMenuMobileOpen')).toHaveLength(1);
      });

    });

  })

  describe('Accordion', () => {
    const accordion = wrapper.find('Accordion');

    it('should have the Accordion component', () => {
      expect(wrapper.find('Accordion')).toHaveLength(1);
    });

    it('should have the icon iconMobileMenu', () => {
      expect(wrapper.find('.classMenuMobileAccordion')).toHaveLength(1);
    });

    it('should have color black', () => {
      expect(accordion.props().color).toEqual('black');
    });
    it('should have iconSize xs', () => {
      expect(accordion.props().iconSize).toEqual('xs');
    });
    it('should have iconOpen icon', () => {
      expect(accordion.props().iconOpen).toEqual('iconArrowUp');
    });
    it('should have iconClosed icon', () => {
      expect(accordion.props().iconClosed).toEqual('iconArrowDown');
    });
    it('should have extendStyleAccordion', () => {
      expect(accordion.props().extendStyle).toEqual('classMenuMobileExtendStyleAccordion');
    });

    describe('Close menu when the click occurs on', () => {
      it('anchor tag', () => {
        const wrapper = mount(<MenuMobile {...props} />);
        wrapper.setState({isMenuOpen: true});
        const accordion = wrapper.find('Accordion');
        const link = accordion.find('a').at(0);
        link.simulate('click');
        expect(wrapper.state().isMenuOpen).toBeFalsy();
      });
      it('NOT an anchor tag', () => {
        const wrapper = mount(<MenuMobile {...props} />);
        wrapper.setState({isMenuOpen: true});
        const link = wrapper.find('li').at(0);
        link.simulate('click');
        expect(wrapper.state().isMenuOpen).toBeTruthy();
      });
    })
  })

  describe('userFullName', () => {

    it('should NOT have an H2 with text Hey Michael Jordan! if user is NOT logged in', () => {
      const h2Tag = wrapper.find('h2');
      expect(h2Tag).toHaveLength(0);
    });
    
    it('should have an H2 with text Hey Michael Jordan! if user is logged in', () => {
      wrapper.setProps({ isUserLoggedIn: true });
      const h2Tag = wrapper.find('h2');
      expect(h2Tag).toHaveLength(1);
      expect(h2Tag.text()).toEqual('Hey Michael Jordan!');
      expect(wrapper.find('.classMenuMobileUserFullName')).toHaveLength(1);
    });
  })

  describe('menuMobileOpenFirstRow', () => {
    
    it('should have a class menuMobileOpenFirstRow', () => {
      expect(wrapper.find('.classMenuMobileFirstRow')).toHaveLength(1);
    });

    describe('menuMobileOpenFirstRow', () => {
      const menuMobileInternalText = wrapper.find('.classMenuMobileInternalText');

      it('should have a class menuMobileInternalText', () => {
        expect(menuMobileInternalText).toHaveLength(1);
      });
  
      it('should have a class menuMobileInternalText', () => {
        expect(menuMobileInternalText.text()).toEqual('Menu');
      });

      describe('MenuCloseIcon', () => {
        
        const menuCloseIcon = wrapper.find('MenuCloseIcon');

        it('should have MenuCloseIcon', () => {
          expect(menuCloseIcon).toHaveLength(1);
        });

        it('should have icon iconMobileMenuClose', () => {
          expect(menuCloseIcon.props().iconMenuClose).toEqual('iconMobileMenuClose')
        });

        it('should have iconSize sm', () => {
          expect(menuCloseIcon.props().iconSize).toEqual('sm')
        });

        describe('When click on MenuCloseIcon', () => {
    
          it('should change state when click on MenuCloseIcon', () => {
            expect(wrapper.state().isMenuOpen).toBeTruthy();
            expect(wrapper.find('.classMenuMobileFirstRow').find('span').at(1).simulate('click'));
            expect(wrapper.state().isMenuOpen).toBeFalsy();
          });
    
        });
        
      });
      
    });

  });
 
  it('should have 2 MenuMobileStaticLinks class', () => {
    expect(wrapper.find('.classMenuMobileStaticLinks')).toHaveLength(2);
  });

  it('should have 2 MenuMobileStaticLink components', () => {
    expect(wrapper.find('MenuMobileStaticLink')).toHaveLength(2);
  });

  it('should have a menuMobile class', () => {
    expect(wrapper.find('.classMenuMobile')).toHaveLength(1);
  });

  describe('addOverflowHiddenWhenMenuIsOpen', () => {

    it('should call the function and add/remove the correct class', () => {

      const classListAddSpy = jest.fn();
      const classListRemoveSpy = jest.fn();
      const bodyMockObj = {
        add: classListAddSpy,
        remove: classListRemoveSpy
      };
      const querySelectorMock = jest.spyOn(document, 'querySelector').mockReturnValueOnce({
        classList: bodyMockObj
      })
      .mockReturnValueOnce({
        classList: bodyMockObj
      });
  
      const wrapper = shallow(<MenuMobile {...props} />);
      
      // the menu is closed
      expect(wrapper.state().isMenuOpen).toBeFalsy();

      // let's open it
      expect(wrapper.find('.classMenuMobileFirstRow').find('span').at(1).simulate('click'));

      expect(wrapper.state().isMenuOpen).toBeTruthy();

      // it needs to call the function once
      expect(querySelectorMock).toHaveBeenCalledTimes(1);

      // call to add with right class
      expect(classListAddSpy).toHaveBeenCalledTimes(1);
      expect(classListAddSpy).toHaveBeenCalledWith(style.classMenuMobileNoSroll);

      expect(classListRemoveSpy).toHaveBeenCalledTimes(0);

      // let's close it
      expect(wrapper.find('.classMenuMobileFirstRow').find('span').at(1).simulate('click'));

      // the state is false
      expect(wrapper.state().isMenuOpen).toBeFalsy();

      // it needs to call the function once
      expect(querySelectorMock).toHaveBeenCalledTimes(2);

      // no more call to classListAddSpy
      expect(classListAddSpy).toHaveBeenCalledTimes(1);
      
      // call to remove now with right class
      expect(classListRemoveSpy).toHaveBeenCalledTimes(1);
      expect(classListRemoveSpy).toHaveBeenCalledWith(style.classMenuMobileNoSroll);

    });
  });

  describe('getFlattenedItems', () => {
    it('external links should be HayLinkNavs', () => {
      const externalVanLinks = {
        flattenInMobile: true,
        navLinks: [
          {
            text: 'Vans',
            href: 'https://www.whatcar.com/class/vans',
            target: '',
            dataCy: 'vans',
            type: 'external'
          }
        ]
      };

      const propsWithExternal = {
        ...props,
        linkComponent: Button,
        data: {
          // to keep the ordering
          Vans: externalVanLinks,
          'Electric & hybrid': props.data['Electric & hybrid']
        }
      }

      const wrapper = shallow(<MenuMobile {...propsWithExternal} />);
      // the first container is the Login / My offers link
      const elem = wrapper.find('.classMenuMobileStaticLinks').at(1);

      // The first two links are our menu items, the last one in this container is the logout button
      expect(elem.children()).toHaveLength(3);

      // This is the one we set to be external
      expect(elem.children().at(0).type()).toBe(HayNavLink);

      // the non-external links are specified through `linkComponent`
      expect(elem.children().at(1).type()).toBe(Button);
    });
  });
});

// @flow
import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Icon from '../../../../Presentational/Icon';
import Accordion from '../../../../Presentational/Accordion';
import MenuCloseIcon from '../MenuCloseIcon';
import MenuElems from '../MenuElems';
import MenuMobileStaticLink from '../MenuMobileStaticLink';
import loginData from '../../data/login';
import replaceUserFullName from '../replaceUserFullName';
import HayNavLink from '../../../../Presentational/HayNavLink';

/**
 *	MenuMobile React Component.
 */
export class MenuMobile extends PureComponent<*, *> {
  /**
   *	Props implementation.
   */
  static propTypes = {
    style: PropTypes.object.isRequired,
    data: PropTypes.object.isRequired,
    linkComponent: PropTypes.func,
    iconArrowDown: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
      .isRequired,
    iconArrowUp: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
      .isRequired,
    iconMobileMenu: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
      .isRequired,
    iconMobileMenuClose: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object
    ]).isRequired,
    isUserLoggedIn: PropTypes.bool.isRequired,
    newCbeUrl: PropTypes.string
  };

  /**
   * Implements defaultProps.
   */
  static defaultProps = {
    data: {},
    style: {},
    newCbeUrl: ''
  };

  static displayName = 'MenuMobile';

  /**
   *	MenuMobile contructor.
   * @param {object} props to constructor.
   *	@constructor
   */
  constructor(props: Object): void {
    super(props);
    this.state = {
      isMenuOpen: false
    };
  }

  /**
   * It adds overflow hidden to the body in order to avoid the background scrolling when the mobile menu is open
   * @returns {void}
   */
  addOverflowHiddenWhenMenuIsOpen = (): void => {
    const {
      style: { classMenuMobileNoSroll }
    } = this.props;

    const body = document.querySelector('body');
    /* istanbul ignore else */
    if (body) {
      if (this.state.isMenuOpen) {
        body.classList.add(classMenuMobileNoSroll);
      } else {
        body.classList.remove(classMenuMobileNoSroll);
      }
    }
  };

  /**
   * It updates the menu state
   * @return {void}
   */
  handleOnClick = (): void =>
    this.setState((prevState): Object => {
      return { isMenuOpen: !prevState.isMenuOpen };
    }, this.addOverflowHiddenWhenMenuIsOpen);

  /**
   * It prepare the content for the accordion component
   * @return {Array} -
   */
  getAccordionContent = (): Array<Object> => {
    const { data, linkComponent, style } = this.props;
    return Object.keys(data)
      .filter(mainCategory => data[mainCategory].navLinks.length !== 1)
      .map(mainCategory => {
        return {
          accordionTitle: mainCategory,
          childContent: (
            <MenuElems
              data={data[mainCategory]}
              style={style}
              linkComponent={linkComponent}
            />
          )
        };
      });
  };

  /**
   * It prepares the elements that appear outside the accordion in mobile view.
   * Used for menus that olny have single items.
   * @returns {Array<ReactElement>} top level items
   */
  getFlattenedItems = (): Array<any> => {
    const { data, linkComponent } = this.props;

    return Object.keys(data)
      .filter(mainCategory => data[mainCategory].navLinks.length === 1)
      .map(category => {
        const link = data[category].navLinks[0];
        const NavLink =
          link && link.type === 'external' ? HayNavLink : linkComponent;
        return (
          <NavLink key={link.href} to={link.href} target={link.target}>
            {link.text}
          </NavLink>
        );
      });
  };

  /**
   * It closes the menu when the click occurs on any accordion's link.
   * @param {HTMLElement} event - HTMLElement
   * @returns {void}
   */
  handleOnClickAccordion = (event: SyntheticInputEvent<*>) => {
    if (event && event.target && event.target.nodeName === 'A') {
      this.setState(
        { isMenuOpen: false },
        this.addOverflowHiddenWhenMenuIsOpen
      );
    }
  };

  /**
   * Render
   * @return {ReactElement} markup
   */
  render() {
    const { isMenuOpen } = this.state;
    const {
      iconMobileMenu,
      iconMobileMenuClose,
      iconArrowUp,
      iconArrowDown,
      userFullName,
      isUserLoggedIn,
      style,
      newCbeUrl
    } = this.props;

    const login = loginData(newCbeUrl).login;

    const {
      classMenuMobile,
      classMenuMobileOpen,
      classMenuMobileExternalText,
      classMenuMobileInternalText,
      classMenuMobileUserFullName,
      classMenuMobileClickable,
      classMenuMobileFirstRow,
      classMenuMobileAccordion,
      classMenuMobileExtendStyleAccordion,
      classMenuMobileStaticLinks,
      ...restOfStyle
    } = style;

    // If the heading is being used in an external partnership page then the icon svgs need to
    // be a hardcoded svg passed in the 'iconSvgObject' prop
    const iconProp =
      typeof iconMobileMenu === 'object'
        ? { iconSvgObject: iconMobileMenu }
        : { iconName: iconMobileMenu };

    return (
      <Fragment>
        <div
          onClick={this.handleOnClick}
          className={classMenuMobileClickable}
          data-cy="mobile-menu-toggle"
        >
          <Icon {...restOfStyle} {...iconProp} size={'lg'} color={'white'} />
          <span className={classMenuMobileExternalText}>Menu</span>
        </div>
        <div
          className={classNames(
            classMenuMobile,
            isMenuOpen ? classMenuMobileOpen : ''
          )}
          data-cy="mobile-menu"
        >
          <div className={classMenuMobileFirstRow}>
            <span className={classMenuMobileInternalText}>Menu</span>
            <span onClick={this.handleOnClick}>
              <MenuCloseIcon
                style={restOfStyle}
                iconMenuClose={iconMobileMenuClose}
                iconSize={'sm'}
              />
            </span>
          </div>
          {isUserLoggedIn && (
            <div className={classMenuMobileUserFullName}>
              <h2>
                {replaceUserFullName(Object.keys(login)[0], userFullName)}
              </h2>
            </div>
          )}
          <div className={classMenuMobileStaticLinks}>
            <MenuMobileStaticLink
              linkType={'mydeals'}
              style={restOfStyle}
              newCbeUrl={newCbeUrl}
              isUserLoggedIn={isUserLoggedIn}
            />
          </div>
          <div
            className={classMenuMobileAccordion}
            onClick={this.handleOnClickAccordion}
          >
            <Accordion
              {...restOfStyle}
              content={this.getAccordionContent()}
              color={'black'}
              iconSize={'xs'}
              iconOpen={iconArrowUp}
              iconClosed={iconArrowDown}
              extendStyle={classMenuMobileExtendStyleAccordion}
            />
          </div>
          <div
            className={classMenuMobileStaticLinks}
            onClick={this.handleOnClick}
          >
            {this.getFlattenedItems()}

            <MenuMobileStaticLink
              linkType={'logout'}
              style={restOfStyle}
              newCbeUrl={newCbeUrl}
              isUserLoggedIn={isUserLoggedIn}
            />
          </div>
        </div>
      </Fragment>
    );
  }
}

export default MenuMobile;

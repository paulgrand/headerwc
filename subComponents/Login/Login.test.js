import React from 'react';
import Login from './index';
import HayNavLink from 'Presentational/HayNavLink';
import style from './login.scss';

const props = {
  userFullName: 'Luca Carangella',
  userInitial: 'LC',
  iconArrowDown: 'icon down',
  iconArrowUp: 'icon up',
  iconLoginClose: 'icon login close',
  linkComponent: HayNavLink,
  loginRef: React.createRef(),
  isMenuOpen: false,
  activeRef: '',
  style: style
};

describe('<Login />', () => {

  const wrapper = mount(<Login {...props} />);

  it('should render Login', () => {
    expect(wrapper).toHaveLength(1);
  });
 
  it('should have userLoggedIn class', () => {
      expect(wrapper.find('.classLoginUserLoggedIn')).toHaveLength(1);
  });

  it('should have MenuDesktop component', () => {
      expect(wrapper.find('MenuDesktop')).toHaveLength(1);
  });

});
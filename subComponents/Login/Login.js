// @flow
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import loginData from '../../data/login'; // this is the content of the login menu
import MenuDesktop from '../MenuDesktop';

/**
 *	Login React Component.
 */
class Login extends PureComponent<*, *> {
  // loginRef = React.createRef();
  /**
   *	Props implementation.
   */
  static propTypes = {
    style: PropTypes.object.isRequired,
    userFullName: PropTypes.string.isRequired,
    userInitial: PropTypes.string.isRequired,
    linkComponent: PropTypes.func,
    loginRef: PropTypes.object.isRequired,
    isUserLoggedIn: PropTypes.bool,
    iconLoginClose: PropTypes.string,
    isMenuOpen: PropTypes.bool,
    activeRef: PropTypes.string,
    newCbeUrl: PropTypes.string
  };
  /**
   * Implements defaultProps.
   */
  static defaultProps = {
    style: {},
    userFullName: 'User',
    userInitial: '?',
    newCbeUrl: ''
  };

  static displayName = 'Login';

  /**
   * Render
   * @return {ReactElement} markup
   */
  render() {
    const {
      iconLoginClose,
      linkComponent,
      userInitial,
      userFullName,
      loginRef,
      isMenuOpen,
      activeRef,
      style,
      newCbeUrl
    } = this.props;

    const login = loginData(newCbeUrl);

    const {
      classLoginUserLoggedIn,
      classLoginMenuSubCategory,
      classLoginUserLoggedOut,
      ...restOfStyle
    } = style;

    return (
      <div className={classLoginUserLoggedIn}>
        <MenuDesktop
          style={restOfStyle}
          data={login}
          iconLoginClose={iconLoginClose}
          linkComponent={linkComponent}
          mainElem={
            <div className={classLoginUserLoggedOut}>{userInitial}</div>
          }
          extendStyleMenuSubCategory={classLoginMenuSubCategory}
          userFullName={userFullName}
          showTriangleUp
          ref={loginRef}
          refPrefix={'nodeLogin'}
          isMenuOpen={isMenuOpen}
          activeRef={activeRef}
          menuDesktopType={'rightMenu'}
        />
        <span>Account</span>
      </div>
    );
  }
}

export default Login;

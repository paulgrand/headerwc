import React from 'react';
import MenuMobileStaticLink from './';

describe('MenuMobileStaticLink', () => {
  let props;

  beforeEach(() => {
    props = {
      linkType: 'mydeals'
    };
  });

  it('should render a MenuMobileStaticLink', () => {
    const MenuMobileStaticLinkComp = shallow(<MenuMobileStaticLink {...props} />);
    expect(MenuMobileStaticLinkComp).toHaveLength(1);
  });

  it('should not render logout if user is not logged in', () => {
    props.linkType = 'logout';
    const MenuMobileStaticLinkComp = shallow(<MenuMobileStaticLink {...props} />);
    expect(MenuMobileStaticLinkComp.html()).toBe('');
  });

  it('should display logout if user is login', () => {
    props.isUserLoggedIn = true;
    props.linkType = 'logout';
    const MenuMobileStaticLinkComp = shallow(<MenuMobileStaticLink {...props} />);
    const menuItem = MenuMobileStaticLinkComp.find('HayNavLink');
    expect(menuItem.childAt(0).text()).toBe('Logout');
  });
});

// @flow
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import rightMenuData from '../../data/rightMenu';
import HayNavLink from '../../../../Presentational/HayNavLink';

/**
 *	MenuMobileStaticLink React Component.
 */
export class MenuMobileStaticLink extends PureComponent<*, *> {
  /**
   *	Props implementation.
   */
  static propTypes = {
    linkType: PropTypes.oneOf(['mydeals', 'logout']).isRequired,
    style: PropTypes.object.isRequired,
    linkComponent: PropTypes.func,
    newCbeUrl: PropTypes.string,
    isUserLoggedIn: PropTypes.bool
  };

  /**
   * Implements defaultProps.
   */
  static defaultProps = {
    style: {},
    linkComponent: HayNavLink,
    newCbeUrl: '',
    isUserLoggedIn: false
  };

  static displayName = 'MenuMobileStaticLink';

  /**
   * Render
   * @return {ReactElement} markup
   */
  render() {
    const { linkType, style, newCbeUrl, isUserLoggedIn } = this.props;
    const { classMenuMobileStaticLinkLogout } = style;
    const rightMenu = rightMenuData(newCbeUrl);

    return (
      <React.Fragment>
        {['mydeals', 'logout']
          .filter(item => item === linkType)
          .filter(item => (isUserLoggedIn ? item : item !== 'logout'))
          .map(section => {
            const { linkComponent } = this.props;
            const NavLink = linkComponent;
            const currentSection = rightMenu[section];
            const text = currentSection.text;
            const classes =
              section === 'logout' ? classMenuMobileStaticLinkLogout : null;

            return (
              <NavLink
                to={currentSection.href}
                key={`${text}`}
                className={classes}
              >
                {text}
              </NavLink>
            );
          })}
      </React.Fragment>
    );
  }
}

export default MenuMobileStaticLink;

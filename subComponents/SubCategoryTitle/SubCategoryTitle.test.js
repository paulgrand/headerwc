import React from 'react';
import SubCategoryTitle from './';
const props = {
  iconLoginClose: 'an icon',
  categoryTitle: 'Category Title'
};

describe('<SubCategoryTitle />', () => {
  const wrapper = shallow(<SubCategoryTitle {...props} />);

  it('should render SubCategoryTitle', () => {
    expect(wrapper).toHaveLength(1);
  });
 
  it('should have a <li>', () => {
    expect(wrapper.find('li')).toHaveLength(1);
  });

  it('should have MenuCloseIcon if the icon is provided', () => {
    expect(wrapper.find('MenuCloseIcon')).toHaveLength(1);
  });

  it('should NOT have MenuCloseIcon if the icon is NOT provided', () => {
    const wrapper = shallow(<SubCategoryTitle categoryTitle={props.categoryTitle}/>);
    expect(wrapper.find('MenuCloseIcon')).toHaveLength(0);
  });

});
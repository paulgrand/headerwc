// @flow
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import MenuCloseIcon from '../MenuCloseIcon';

/**
 *	SubCategoryTitle React Component.
 */
export class SubCategoryTitle extends PureComponent<*, *> {
  /**
   *	Props implementation.
   */
  static propTypes = {
    iconLoginClose: PropTypes.string,
    categoryTitle: PropTypes.string.isRequired,
    style: PropTypes.object.isRequired
  };

  /**
   * Implements defaultProps.
   */
  static defaultProps = {
    style: {}
  };

  static displayName = 'SubCategoryTitle';

  /**
   * Render
   * @return {ReactElement} markup
   */
  render() {
    const { categoryTitle, iconLoginClose, style } = this.props;
    const { subCategoryTitle, ...restOfStyle } = style;

    return (
      <li className={subCategoryTitle}>
        <span>{categoryTitle}</span>
        {iconLoginClose && (
          <MenuCloseIcon style={restOfStyle} iconMenuClose={iconLoginClose} />
        )}
      </li>
    );
  }
}

export default SubCategoryTitle;

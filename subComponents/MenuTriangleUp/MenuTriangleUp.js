// @flow
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

/**
 *	MenuTriangleUp React Component.
 */
export class MenuTriangleUp extends PureComponent<*, *> {
  /**
   *	Props implementation.
   */
  static propTypes = {
    showTriangle: PropTypes.bool,
    style: PropTypes.object.isRequired
  };
  /**
   * Implements defaultProps.
   */
  static defaultProps = {
    style: {},
    showTriangle: false
  };

  static displayName = 'MenuTriangleUp';

  /**
   * Render
   * @return {ReactElement} markup
   */
  render() {
    const { showTriangle, style } = this.props;
    const { classMenuTriangleUpContainer, classMenuTriangleUp } = style;

    return (
      <div
        className={classNames(
          classMenuTriangleUpContainer,
          showTriangle ? style.showTriangle : ''
        )}
      >
        <span className={classMenuTriangleUp} />
      </div>
    );
  }
}

export default MenuTriangleUp;

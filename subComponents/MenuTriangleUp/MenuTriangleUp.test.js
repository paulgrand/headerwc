import React from 'react';
import MenuTriangleUp from './';
import style from './menuTriangleUp.scss';

const props = {
  showTriangle: false,
  style: style
};

describe('<MenuTriangleUp />', () => {
  const wrapper = shallow(<MenuTriangleUp {...props} />);

  it('should render MenuTriangleUp', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('should have a div with a class classMenuTriangleUpContainer', () => {
    expect(wrapper.find('div').hasClass('classMenuTriangleUpContainer')).toBeTruthy();
  });

  it('should NOT have a div with a class showTriangle if show Triangle is false', () => {
    expect(wrapper.find('div').hasClass('showTriangle')).toBeFalsy();
  });

  it('should have a div with a class showTriangle if show Triangle is true', () => {
    wrapper.setProps({ showTriangle: true });
    expect(wrapper.find('div').hasClass('showTriangle')).toBeTruthy();
  });
 
  it('should have a span with a class classMenuTriangleUp', () => {
    expect(wrapper.find('span').hasClass('classMenuTriangleUp')).toBeTruthy();
  });


});
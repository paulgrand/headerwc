// @flow
/**
 * It replaces the userFullName with with something else
 * @param {string} replaceFrom - string you want to apply the replacement to
 * @param {string} replaceWith - string you want to replace %userFullName with
 * @returns {string} - new string with %userFullName replaced.
 */
const replaceUserFullName = (
  replaceFrom: string,
  replaceWith: string
): string => replaceFrom.replace('%userFullName', replaceWith);

export default replaceUserFullName;

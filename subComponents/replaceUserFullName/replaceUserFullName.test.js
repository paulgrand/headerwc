import replaceUserFullName from './';

describe('replaceUserFullName', () => {
  
  it('should replace the user full name', () => {
    const strFrom = 'Hey %userFullName!!';
    expect(replaceUserFullName(strFrom, 'Michael Jordan')).toEqual('Hey Michael Jordan!!');
  });
})
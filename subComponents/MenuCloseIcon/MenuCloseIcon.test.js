import React from 'react';
import MenuCloseIcon from './';
const props = {
  iconMenuClose: 'an icon'
};
describe('<MenuCloseIcon />', () => {
  const wrapper = shallow(<MenuCloseIcon {...props} />);
  it('should render MenuCloseIcon', () => {
    expect(wrapper).toHaveLength(1);
  });
 
 it('should have an Icon', () => {
  expect(wrapper.find('Icon')).toHaveLength(1);
 });

 it('should NOT have an Icon if the icon is not provided and return null', () => {
  console.error = jest.fn();
  const wrapper = shallow(<MenuCloseIcon />);
  expect(wrapper.find('Icon')).toHaveLength(0);
  expect(wrapper.type()).toBeNull();
 });

 it('should accept a prop "iconMenuClose" as an object', () => {
  const wrapper = shallow(<MenuCloseIcon iconMenuClose={{}} />);
  expect(wrapper.find('Icon').prop('iconSvgObject')).toEqual({});
 });

});
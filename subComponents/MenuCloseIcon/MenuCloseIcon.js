// @flow
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Icon from '../../../../Presentational/Icon';

/**
 *	MenuCloseIcon React Component.
 */
export class MenuCloseIcon extends PureComponent<*, *> {
  /**
   *	Props implementation.
   */
  static propTypes = {
    iconMenuClose: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
      .isRequired,
    style: PropTypes.object.isRequired
  };

  /**
   * Implements defaultProps.
   */
  static defaultProps = {
    style: {}
  };

  static displayName = 'MenuCloseIcon';

  /**
   * Render
   * @return {ReactElement} markup
   */
  render() {
    const { iconMenuClose, style } = this.props;
    const { classMenuCloseIconClickable, ...restOfStyle } = style;

    // If the heading is being used in an external partnership page then the icon svgs need to
    // be a hardcoded svg passed in the 'iconSvgObject' prop
    const iconProp =
      typeof iconMenuClose === 'object'
        ? { iconSvgObject: iconMenuClose }
        : { iconName: iconMenuClose };

    return iconMenuClose ? (
      <Icon
        {...restOfStyle}
        {...iconProp}
        size={'xxs'}
        extendStyle={classMenuCloseIconClickable}
      />
    ) : null;
  }
}

export default MenuCloseIcon;

// @flow
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import HayNavLink from '../../../../Presentational/HayNavLink';

/**
 *	MenuSubElem React component.
 */
export class MenuSubElem extends PureComponent<*, *> {
  /**
   *	Props implementation.
   */
  static propTypes = {
    elem: PropTypes.shape({
      href: PropTypes.string.isRequired,
      target: PropTypes.string,
      text: PropTypes.string.isRequired,
      type: PropTypes.string
    }).isRequired,
    linkComponent: PropTypes.func,
    style: PropTypes.object,
    index: PropTypes.number
  };

  /**
   * Implements defaultProps.
   */
  static defaultProps = {
    style: {},
    linkComponent: HayNavLink
  };

  static displayName = 'MenuSubElem';

  /**
   * Render
   * @return {ReactElement} markup
   */
  render() {
    const { elem, linkComponent, style } = this.props;
    const { classMenuSubElem, classMenuSubElemActive } = style;

    const NavLink =
      elem && elem.type && elem.type === 'external'
        ? HayNavLink
        : linkComponent;

    return elem ? (
      <li className={classMenuSubElem} data-cy={elem.dataCy}>
        {
          <NavLink
            to={elem.href}
            target={elem.target}
            activeClassName={classMenuSubElemActive}
          >
            {elem.text}
          </NavLink>
        }
      </li>
    ) : null;
  }
}

export default MenuSubElem;

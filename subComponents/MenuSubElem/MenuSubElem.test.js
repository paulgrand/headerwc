import React from 'react';
import MenuSubElem from './';

const props = {
  elem: {
    href: '/wherever',
    target: '_blank',
    text: 'some text'
  }
};

describe('<MenuSubElem />', () => {
  
  const wrapper = shallow(<MenuSubElem {...props} />);

  it('should render MenuSubElem', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('should render null is no elem is provided', () => {
    console.error = jest.fn();
    const wrapper = shallow(<MenuSubElem />);
    expect(wrapper.type()).toBeNull();
  });

  it('should have a NavLink', () => {
    expect(wrapper.find('HayNavLink')).toHaveLength(1);
  });

  it('should have a right text', () => {
    const wrapper = mount(<MenuSubElem {...props} />);
    expect(wrapper.text()).toEqual(props.elem.text);
  });

  it('should have a <li> tag', () => {
      expect(wrapper.find('li')).toHaveLength(1);
  });
});

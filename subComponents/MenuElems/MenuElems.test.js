import React from 'react';
import MenuElems from './';
import style from './menuElems.scss';

const props = {
  data: {
    "sub Category": [
      {
        text: 'some text 1',
        href: 'www.anicewebsite1.com',
        target: ''
      },
      {
        text: 'some text 2',
        href: 'www.anicewebsite2.com',
        target: ''
      },
    ]
  },
  iconLoginClose: 'an icon',
  style: style,
  menuSubCategoryOverride: 'sub Category'
};

describe('<MenuElems />', () => {

  it('should return null with data is not providfed', () => {
    console.error = jest.fn();
    const wrapper = shallow(<MenuElems />);
    expect(wrapper.type()).toBeNull();
  });

  const wrapper = mount(<MenuElems {...props} />);

  it('should have a <ul> with the class menuElems', () => {
    expect(wrapper.find('.classMenuElems')).toHaveLength(1);
  });

  it('should have 1 <MenuSubElems>', () => {
    expect(wrapper.find('MenuSubElems')).toHaveLength(1);
  });

  it('should have 1 <SubCategoryTitle>', () => {
    expect(wrapper.find('SubCategoryTitle')).toHaveLength(1);
  });

  it('should have "sub Category" as text', () => {
    expect(wrapper.find('SubCategoryTitle').text()).toEqual('sub Category');
  });

});
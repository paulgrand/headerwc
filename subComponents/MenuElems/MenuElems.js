// @flow
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import MenuSubElems from '../MenuSubElems';
import SubCategoryTitle from '../SubCategoryTitle';

/**
 *	MenuElems React Component.
 */
class MenuElems extends PureComponent<*, *> {
  /**
   *	Props implementation.
   */
  static propTypes = {
    data: PropTypes.objectOf(
      PropTypes.arrayOf(
        PropTypes.shape({
          text: PropTypes.string,
          href: PropTypes.string,
          target: PropTypes.string
        })
      )
    ),
    style: PropTypes.object.isRequired,
    linkComponent: PropTypes.func,
    menuSubCategoryOverride: PropTypes.string,
    iconLoginClose: PropTypes.string
  };

  /**
   * Implements defaultProps.
   */
  static defaultProps = {
    style: {},
    data: {}
  };

  static displayName = 'MenuElems';

  /**
   * Render
   * @return {ReactElement} markup
   */
  render(): Array<*> | null {
    const {
      data,
      linkComponent,
      menuSubCategoryOverride,
      iconLoginClose,
      style
    } = this.props;

    const { classMenuElems, ...restOfStyle } = style;

    if (data && Object.keys(data).length === 0) {
      return null;
    }

    return Object.keys(data).map((menuSubCategory, index) => {
      return (
        <ul
          className={classMenuElems}
          key={`sub-${menuSubCategory}`}
          data-cy={`sub-category-${index}`}
        >
          {menuSubCategoryOverride && (
            <SubCategoryTitle
              style={restOfStyle}
              categoryTitle={menuSubCategoryOverride}
              iconLoginClose={iconLoginClose}
            />
          )}
          <MenuSubElems
            style={restOfStyle}
            elems={data[menuSubCategory]}
            linkComponent={linkComponent}
          />
        </ul>
      );
    });
  }
}

export default MenuElems;

import React from 'react';
import MenuMainElem from './';

const props = {
  menuMainCategoryText: 'some text',
  iconName: 'an icon',
  menuMainCategoryLink: {
    href: 'someURL',
    target: '_blank'
  }
};

describe('<MenuMainElem /> NavLink', () => {
  const wrapper = shallow(<MenuMainElem {...props} />);

  it('should render MenuMainElem', () => {
    expect(wrapper).toHaveLength(1);
  });
 
  it('should have an Icon ', () => {
    expect(wrapper.find('Icon')).toHaveLength(1);
  });

  describe('Link elem', () => {
    const link = wrapper.find('HayNavLink').childAt(0);
    
    it('should exist', () => {
      expect(link).toHaveLength(1);
    });

    it('should have a text and icon', () => {
      expect(link.text()).toContain(props.menuMainCategoryText);
    });

    it('should be clickable for tracking', () => {
      const link = wrapper.find('HayNavLink');
      link.simulate('click');
    });
  })

});

describe('<MenuMainElem /> HayNavLink', () => {

  const newProps = Object.assign({}, props, {menuMainCategoryLink: {
    href: 'someURL',
    target: ''
  }});

  const wrapper = shallow(<MenuMainElem {...newProps} />);

  it('should render MenuMainElem', () => {
    expect(wrapper).toHaveLength(1);
  });

});
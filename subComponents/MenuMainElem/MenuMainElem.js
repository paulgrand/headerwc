// @flow
import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import Icon from '../../../../Presentational/Icon';
import HayNavLink from '../../../../Presentational/HayNavLink';

/**
 *	MenuMainElem React Component.
 */
export class MenuMainElem extends PureComponent<*, *> {
  /**
   *	Props implementation.
   */
  static propTypes = {
    menuMainCategoryText: PropTypes.string.isRequired,
    menuMainCategoryLink: PropTypes.object,
    linkComponent: PropTypes.func,
    iconName: PropTypes.string,
    iconSvgObject: PropTypes.object,
    style: PropTypes.object
  };

  /**
   * Implements defaultProps.
   */
  static defaultProps = {
    style: {},
    linkComponent: HayNavLink
  };

  /**
   * triggerEvent func
   * @return {void} pushes to dataLayer
   */
  triggerEvent = () => {
    try {
      let eventLabel = this.props && this.props.menuMainCategoryText;
      let eventAction = 'Top Menu Nav';
      let eventCategory = 'Navigation - Next Gen';
      window.dataLayer.push({
        event: 'customEvent',
        eventCategory: eventCategory,
        eventAction: eventAction,
        eventLabel: eventLabel
      });
    } catch (error) {}
  };

  static displayName = 'MenuMainElem';

  /**
   * Render
   * @return {ReactElement} markup
   */
  render() {
    const {
      menuMainCategoryText,
      menuMainCategoryLink,
      linkComponent,
      iconName,
      iconSvgObject,
      style
    } = this.props;

    const { href, type, target } = menuMainCategoryLink;

    const NavLink = type === 'external' ? HayNavLink : linkComponent;

    // If the heading is being used in an external partnership page then the icon svgs need to
    // be a hardcoded svg passed in the 'iconSvgObject' prop
    const iconProp =
      typeof iconSvgObject === 'object'
        ? { iconSvgObject: iconSvgObject }
        : { iconName: iconName };

    return (
      <NavLink to={href} target={target} onClick={this.triggerEvent}>
        <Fragment>
          {menuMainCategoryText}
          <Icon {...style} {...iconProp} size={'micro'} color={'white'} />
        </Fragment>
      </NavLink>
    );
  }
}

export default MenuMainElem;

// @flow
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import MenuElems from '../MenuElems';
import MenuMainElem from '../MenuMainElem';
import MenuTriangleUp from '../MenuTriangleUp';
import replaceUserFullName from '../replaceUserFullName';

/**
 *	MenuDesktop React component.
 */
export class MenuDesktop extends PureComponent<*, *> {
  /**
   *	Props implementation.
   */
  static propTypes = {
    data: PropTypes.object.isRequired,
    iconArrowDown: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    iconArrowUp: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    iconLoginClose: PropTypes.string,
    isMenuOpen: PropTypes.bool,
    activeRef: PropTypes.string.isRequired,
    style: PropTypes.object.isRequired,
    refPrefix: PropTypes.string,
    mainElem: PropTypes.object,
    showTriangleUp: PropTypes.bool,
    extendStyleMenuSubCategory: PropTypes.string,
    userFullName: PropTypes.string,
    linkComponent: PropTypes.func,
    menuDesktopType: PropTypes.oneOf(['leftMenu', 'rightMenu'])
  };

  /**
   * Implements defaultProps.
   */
  static defaultProps = {
    style: {},
    data: [],
    refPrefix: 'NodeMain',
    showTriangleUp: false,
    isMenuOpen: false,
    activeRef: '',
    menuDesktopType: 'leftMenu'
  };

  static displayName = 'MenuDesktop';

  /**
   * @param {string} nodeName - name to give to the ref
   * @param {HTMLElement} nodeRef - html elemenent
   * @return {void}
   */
  setNodeRef = (nodeName: string, nodeRef: ?HTMLElement): void => {
    // At the moment is not possible to define dynamic type for ref.
    // $FlowFixMe
    this[nodeName] = nodeRef;
  };

  /**
   * @param {HTMLElement} currentRef: html elemenent
   * @return {boolean} - is the Menu open && is the active ref in the state the same as the currentRef which has been clicked.
   */
  isOpenAndActiveRef = (currentRef: ?HTMLElement) => {
    const { isMenuOpen, activeRef } = this.props;

    if (currentRef) {
      return isMenuOpen && activeRef === currentRef.id;
    }
    return false;
  };

  /**
   * @param {HTMLElement} currentRef: html elemenent
   * @return {boolean} - is the Menu open && is the active ref in the state IS NOT the same as the currentRef which has been clicked.
   */
  isOpenButNotActiveRef = (currentRef: ?HTMLElement) => {
    const { isMenuOpen, activeRef } = this.props;

    if (currentRef) {
      return isMenuOpen && activeRef !== currentRef.id;
    }
    return false;
  };

  /**
   * @param {HTMLElement} currentRef: html elemenent
   * @returns {string} openClass - class to return
   */
  getOpenMenuClass = (currentRef: ?HTMLElement) => {
    const { style, menuDesktopType } = this.props;
    const {
      classMenuDesktopOpen,
      classMenuDesktopMenuIsOpenButIAmNotActive
    } = style;

    const openClass = this.isOpenAndActiveRef(currentRef)
      ? classMenuDesktopOpen // It gives a class to the active ref when the menu is open.
      : menuDesktopType === 'leftMenu' && this.isOpenButNotActiveRef(currentRef) // It applies only to the left menu
        ? classMenuDesktopMenuIsOpenButIAmNotActive // It gives another class to all the other elems that are not the active one when the menu is open.
        : ''; // The menu is closed.
    return openClass;
  };

  /**
   * @param {object} data - Menu's element from JSON/fetch
   * @return {JSX} -
   */
  getMenu = (data: Object): Array<*> => {
    const {
      refPrefix,
      iconArrowUp,
      iconArrowDown,
      iconLoginClose,
      linkComponent,
      mainElem,
      showTriangleUp,
      extendStyleMenuSubCategory,
      userFullName,
      style
    } = this.props;

    const {
      classMenuDesktopMainCategory,
      classMenuDesktopMainCategoryTitleContainer,
      classMenuDesktopSubCategory,
      classMenuDesktopOpen,
      classMenuDesktopMenuIsOpenButIAmNotActive,
      ...restOfStyle
    } = style;

    return Object.keys(data).map((menuMainCategory, index) => {
      const currentRefName = `DOMRef${refPrefix}${index}`;
      // At the moment is not possible to define dynamic type for ref.
      // $FlowFixMe
      const currentRef = this[currentRefName];

      const firstSubMenuCategoryName =
        userFullName && Object.keys(data[menuMainCategory])[0];

      const menuMainCategoryLink =
        data &&
        data[menuMainCategory] &&
        data[menuMainCategory].navLinks &&
        data[menuMainCategory].navLinks[0];

      const showDropDownArrowAndMenu =
        (data &&
          data[menuMainCategory] &&
          data[menuMainCategory].navLinks &&
          data[menuMainCategory].navLinks.length > 1) ||
        refPrefix === 'nodeLogin';

      const arrowIcon = showDropDownArrowAndMenu
        ? this.isOpenAndActiveRef(currentRef)
          ? iconArrowUp
          : iconArrowDown
        : ' ';

      // If the heading is being used in an external partnership page then the icon svgs need to
      // be a hardcoded svg passed in the 'iconSvgObject' prop
      const iconProp =
        typeof arrowIcon === 'object'
          ? { iconSvgObject: arrowIcon }
          : { iconName: arrowIcon };

      return (
        <div
          className={classNames(
            classMenuDesktopMainCategory,
            this.getOpenMenuClass(currentRef)
          )}
          ref={node => this.setNodeRef(currentRefName, node)}
          key={`main-${menuMainCategory}-${index}`}
          id={`DOMRef${refPrefix}${index}`}
          data-cy={`menu-main-category-${index}`}
        >
          <div
            className={classMenuDesktopMainCategoryTitleContainer}
            data-cy={`menu-popout-link`}
          >
            {mainElem || (
              <MenuMainElem
                style={restOfStyle}
                linkComponent={linkComponent}
                menuMainCategoryText={menuMainCategory}
                menuMainCategoryLink={menuMainCategoryLink}
                {...iconProp}
              />
            )}
            {showTriangleUp && (
              <MenuTriangleUp
                style={restOfStyle}
                showTriangle={this.isOpenAndActiveRef(currentRef)}
              />
            )}
          </div>
          {showDropDownArrowAndMenu && (
            <div
              className={classNames(
                classMenuDesktopSubCategory,
                extendStyleMenuSubCategory
              )}
            >
              <MenuElems
                style={restOfStyle}
                data={data[menuMainCategory]}
                menuSubCategoryOverride={
                  userFullName &&
                  replaceUserFullName(firstSubMenuCategoryName, userFullName)
                }
                linkComponent={linkComponent}
                iconLoginClose={iconLoginClose}
              />
            </div>
          )}
        </div>
      );
    });
  };

  /**
   * Render implementation.
   * @return {ReactElement} markup
   */
  render() {
    const { data, style } = this.props;
    const { classMenuDesktop } = style;

    return data && Object.keys(data).length > 0 ? (
      <div className={classMenuDesktop}>{this.getMenu(data)}</div>
    ) : null;
  }
}

export default MenuDesktop;

import React from 'react';
import MenuDesktop from './';
import data from '../../data/leftMenu';
import style from './menuDesktop.scss';

const someUrl = 'www.hello.com';
const dataObj = data(someUrl);

const props = {
  data: dataObj,
  iconArrowDown: 'icon iconArrowDown',
  iconArrowUp: 'icon iconArrowUp',
  iconLoginClose: 'icon loginClose',
  style: style
};

describe('<MenuDesktop />', () => {
  
  const wrapper = shallow(<MenuDesktop {...props} />);

  it('should render MenuDesktop', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('should have class menuMainCategory', () => {
    expect(wrapper.find('.classMenuDesktopMainCategory')).toHaveLength(7);
  });

  describe('<MenuTriangleUp>', () => {
    it('should NOT be present if showTriangleUp is false', () => {
      expect(wrapper.find('MenuTriangleUp')).toHaveLength(0)
    });

    it('should be present if showTriangleUp is true', () => {
      wrapper.setProps({showTriangleUp: true})
      expect(wrapper.find('MenuTriangleUp')).toHaveLength(7) // one each for every mainCategory.
    });
  })

  describe('<MenuMainElem>', () => {
    it('should be present if mainElem is not provided', () => {
      expect(wrapper.find('MenuMainElem')).toHaveLength(7);
      expect(wrapper.text()).not.toContain('whatever');
    });
    it('should render what is passed in the mainElem prop if it is provided', () => {
      wrapper.setProps({mainElem: <p>whatever</p>})
      expect(wrapper.text()).toContain('whatever');
    });
    
    it('should have class menuMainCategoryTitleContainer', () => {
      expect(wrapper.find('.classMenuDesktopMainCategoryTitleContainer')).toHaveLength(7);
    });
  })

  it('should have <MenuElems>', () => {
    expect(wrapper.find('MenuElems')).toHaveLength(5)
  });
  
  it('should have a class open the elem DOMRefNodeMain0 when header sends right isMenuOpen && activeRef', () => {
    const wrapper = mount(<MenuDesktop {...props} />);

    wrapper.setProps({
      isMenuOpen: true,
      activeRef: 'DOMRefNodeMain0'
    })

    expect(wrapper.find('.classMenuDesktopOpen')).toHaveLength(1);
    
  });

  it('should accept icon prop(s) as objects instead of strings', () => {
    const wrapper = mount(<MenuDesktop {...props} iconArrowDown={{}} />);
    expect(wrapper.find('MenuMainElem').first().prop('iconSvgObject')).toEqual({});
  });

  it('should render null if data is an empty obj provided', () => {
    const wrapper = shallow(<MenuDesktop {...props} data={{}} />);
    expect(wrapper.type()).toBeNull();
  });

  it('should render null if data is not provided', () => {
    console.error = jest.fn();
    const wrapper = shallow(<MenuDesktop {...props} data={null} />);
    expect(wrapper.type()).toBeNull();
  });
});
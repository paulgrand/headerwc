import React from 'react';
import OverlayWhenMenuIsOpen from './';
import style from './OverlayWhenMenuIsOpen.scss';

const props = {
  style: style
};

describe('<OverlayWhenMenuIsOpen />', () => {
  const wrapper = shallow(<OverlayWhenMenuIsOpen {...props} />);

  it('should render OverlayWhenMenuIsOpen', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('should have a class classOverlayWhenMenuIsOpen', () => {
    expect(wrapper.find('.classOverlayWhenMenuIsOpen')).toHaveLength(1);
  });

  it('should have a class classOverlayWhenMenuIsOpenVisible is isMenuOpen is true', () => {
    const wrapper = shallow(<OverlayWhenMenuIsOpen {...props} isMenuOpen={true} />);
    expect(wrapper.find('.classOverlayWhenMenuIsOpenVisible')).toHaveLength(1);
  });

  it('should NOT have a class classOverlayWhenMenuIsOpenVisible is isMenuOpen is false', () => {
    const wrapper = shallow(
      <OverlayWhenMenuIsOpen {...props} isMenuOpen={false} />
    );
    expect(wrapper.find('.classOverlayWhenMenuIsOpenVisible')).toHaveLength(0);
  });
});

// @flow
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

/**
 *	OverlayWhenMenuIsOpen React Component.
 */
export class OverlayWhenMenuIsOpen extends PureComponent<*, *> {
  /**
   *	Props implementation.
   */
  static propTypes = {
    style: PropTypes.object.isRequired,
    isMenuOpen: PropTypes.bool.isRequired
  };
  /**
   * Implements defaultProps.
   */
  static defaultProps = {
    style: {},
    isMenuOpen: false
  };

  static displayName = 'OverlayWhenMenuIsOpen';

  /**
   * Render
   * @return {ReactElement} markup
   */
  render() {
    const { isMenuOpen, style } = this.props;
    const {
      classOverlayWhenMenuIsOpen,
      classOverlayWhenMenuIsOpenVisible
    } = style;

    return (
      <div
        className={classNames(
          classOverlayWhenMenuIsOpen,
          isMenuOpen ? classOverlayWhenMenuIsOpenVisible : ''
        )}
      />
    );
  }
}

export default OverlayWhenMenuIsOpen;

// @flow
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import InfoIcon from '../../../../Presentational/InfoIcon';
import Login from '../Login/index';
import rightMenuData from '../../data/rightMenu';
import HayNavLink from '../../../../Presentational/HayNavLink';
import classNames from 'classnames';

/**
 *  InfoIconsList React Component.
 @returns {jsx} -
 */
export class InfoIconsList extends PureComponent<*, *> {
  /**
   *  Props implementation.
   */
  static propTypes = {
    style: PropTypes.object.isRequired,
    userFullName: PropTypes.string.isRequired,
    userInitial: PropTypes.string.isRequired,
    iconMyDeals: PropTypes.string.isRequired,
    iconAvatar: PropTypes.string.isRequired,
    iconSearch: PropTypes.string,
    iconLoginClose: PropTypes.string,
    isUserLoggedIn: PropTypes.bool,
    linkComponent: PropTypes.func,
    loginRef: PropTypes.object,
    isMenuOpen: PropTypes.bool,
    activeRef: PropTypes.string,
    newCbeUrl: PropTypes.string,
    extendStyle: PropTypes.string
  };

  /**
   * Implements defaultProps.
   */
  static defaultProps = {
    style: {},
    newCbeUrl: ''
  };

  static displayName = 'InfoIconsList';

  getInfoIcons = (): Array<*> => {
    const {
      iconMyDeals,
      iconSearch,
      iconAvatar,
      iconLoginClose,
      isUserLoggedIn,
      userFullName,
      userInitial,
      loginRef,
      isMenuOpen,
      activeRef,
      style,
      newCbeUrl
    } = this.props;

    const {
      classInfoIconsListElem,
      classInfoIconsListLogin,
      classInfoIconsListSearch,
      classInfoIconsListMyDeals,
      classInfoIconsListAvatar,
      ...restOfStyle
    } = style;

    const rightMenu = rightMenuData(newCbeUrl);

    const infoIcons = [
      {
        url: rightMenu.mydeals.href,
        target: rightMenu.mydeals.target,
        type: rightMenu.mydeals.type,
        comp: iconMyDeals ? (
          <div
            className={classNames(
              classInfoIconsListElem,
              classInfoIconsListMyDeals
            )}
          >
            <InfoIcon
              {...restOfStyle}
              iconName={iconMyDeals}
              caption={'My offers'}
              circleColour={'transparent'}
              circleSize={'xs'}
              color={'white'}
            />
          </div>
        ) : null
      },
      {
        url: rightMenu.login.href,
        target: rightMenu.login.target,
        type: rightMenu.login.type,
        comp: iconAvatar ? (
          isUserLoggedIn ? null : (
            <div className={classInfoIconsListElem} key={`key-avatar`}>
              <InfoIcon
                {...restOfStyle}
                iconName={iconAvatar}
                caption={'Account'}
                circleColour={'transparent'}
                circleSize={'xs'}
                iconSize={'md'}
                color={'white'}
              />
            </div>
          )
        ) : null
      },
      {
        url: null,
        target: '',
        comp: isUserLoggedIn ? (
          <div className={classInfoIconsListElem} key={`key-login`}>
            <Login
              newCbeUrl={newCbeUrl}
              style={restOfStyle}
              iconLoginClose={iconLoginClose}
              userFullName={userFullName}
              userInitial={userInitial}
              loginRef={loginRef}
              isMenuOpen={isMenuOpen}
              activeRef={activeRef}
            />
          </div>
        ) : null
      },
      {
        url: rightMenu.search.href,
        target: rightMenu.search.target,
        comp: iconSearch ? (
          <div className={classInfoIconsListElem} key={`key-search`}>
            <InfoIcon
              {...restOfStyle}
              iconName={iconSearch}
              caption={'Search'}
              circleColour={'transparent'}
              circleSize={'xs'}
              iconSize={'xlg'}
              color={'white'}
            />
          </div>
        ) : null
      }
    ];
    return infoIcons.map((elem, index) => {
      const { linkComponent } = this.props;
      const url = elem.url;
      const type = elem.type || null;
      const component = elem.comp;
      const target = elem.target;

      if (url) {
        const NavLink = type === 'external' ? HayNavLink : linkComponent;

        return component ? (
          <NavLink to={url} target={target} key={`key-${url}${index}`}>
            {component}
          </NavLink>
        ) : null;
      }

      return component || null;
    });
  };

  /**
   * Render
   * @return {ReactElement} markup
   */
  render() {
    const {
      iconMyDeals,
      iconSearch,
      iconAvatar,
      style,
      extendStyle
    } = this.props;
    const { classInfoIconsList } = style;
    return iconMyDeals || iconAvatar || iconSearch ? (
      <div className={classNames(classInfoIconsList, extendStyle)}>
        {this.getInfoIcons()}
      </div>
    ) : null;
  }
}

export default InfoIconsList;

import React from 'react';
import InfoIconsList from './';
import HayNavLink from 'Presentational/HayNavLink';
import style from './infoIconsList.scss';

const icons = {
  iconMyDeals: 'iconMyDeals',
  iconAvatar: 'iconAvatar',
  iconSearch: 'iconSearch'
};

const props = {
  userFullName: 'Luca Carangella',
  userInitial: 'LC',
  linkComponent: HayNavLink,
  style: style,
  ...icons
};

describe('<InfoIconsList />', () => {
  it('should render InfoIconsList with 3 InfoIcons', () => {
    const wrapper = shallow(<InfoIconsList {...props} />);
    expect(wrapper).toHaveLength(1);
    expect(wrapper.find('InfoIcon')).toHaveLength(3);
  });

  it('should have a my deals InfoIcon ', () => {
    const wrapper = shallow(<InfoIconsList {...props} />);
    expect(wrapper.find('InfoIcon').at(0).props().iconName).toEqual(icons.iconMyDeals);
    expect(wrapper.find('InfoIcon').at(0).props().circleColour).toEqual('transparent');
    expect(wrapper.find('InfoIcon').at(0).props().circleSize).toEqual('xs');
    expect(wrapper.find('InfoIcon').at(0).props().iconSize).toEqual('lg');
    expect(wrapper.find('InfoIcon').at(0).props().color).toEqual('white');
  });

  it('should have an avatar InfoIcon ', () => {
    const wrapper = shallow(<InfoIconsList {...props} />);
    expect(wrapper.find('InfoIcon').at(1).props().iconName).toEqual(icons.iconAvatar);
    expect(wrapper.find('InfoIcon').at(1).props().circleColour).toEqual('transparent');
    expect(wrapper.find('InfoIcon').at(1).props().circleSize).toEqual('xs');
    expect(wrapper.find('InfoIcon').at(1).props().iconSize).toEqual('md');
    expect(wrapper.find('InfoIcon').at(1).props().color).toEqual('white');
  });

  it('should have a search InfoIcon ', () => {
    const wrapper = shallow(<InfoIconsList {...props} />);
    expect(wrapper.find('InfoIcon').at(2).props().iconName).toEqual(icons.iconSearch);
    expect(wrapper.find('InfoIcon').at(2).props().circleColour).toEqual('transparent');
    expect(wrapper.find('InfoIcon').at(2).props().circleSize).toEqual('xs');
    expect(wrapper.find('InfoIcon').at(2).props().borderColor).toBeUndefined(); // no border
    expect(wrapper.find('InfoIcon').at(2).props().iconSize).toEqual('xlg');
    expect(wrapper.find('InfoIcon').at(2).props().color).toEqual('white');
  });

  it('should return null if iconMyDeals || iconAvatar || iconSearch are not provided', () => {
    console.error = jest.fn();
    const wrapper = shallow(<InfoIconsList />);
    expect(wrapper.type()).toBeNull();
  });


  ['iconMyDeals', 'iconSearch', 'iconAvatar'].forEach( (elem, index) => {

    it(`should NOT have the InfoIcon ${elem} component if the icon for ${elem} is not provided`, () => {
      console.error = jest.fn();
      const newProps = {
        ...props,
        [elem]: undefined
      }

      const wrapper = shallow(<InfoIconsList {...newProps} />);
      const infoIconsArr = wrapper.find('InfoIcon');
  
      infoIconsArr.forEach( (element,index) => {
        expect(wrapper.find('InfoIcon').at(index).props().iconName).not.toEqual(icons[elem]);
      });
      
    });
  })

});

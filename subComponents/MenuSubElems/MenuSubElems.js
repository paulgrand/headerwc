// @flow
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import MenuSubElem from '../MenuSubElem';

/**
 *	MenuSubElems React Component.
 */
export class MenuSubElems extends PureComponent<*, *> {
  /**
   *	Props implementation.
   */
  static propTypes = {
    elems: PropTypes.array.isRequired,
    linkComponent: PropTypes.func,
    style: PropTypes.object
  };

  /**
   * Implements defaultProps.
   */
  static defaultProps = {
    style: {},
    elems: []
  };

  static displayName = 'MenuSubElems';

  /**
   * Render
   * @return {ReactElement} markup
   */
  render() {
    const { elems, linkComponent, style } = this.props;
    const { menuSubElems, ...restOfStyle } = style;

    if (elems && elems.length === 0) {
      return null;
    }

    return (
      <ul className={menuSubElems}>
        {elems.map(menuElem => {
          return (
            <MenuSubElem
              style={restOfStyle}
              key={`${menuElem.text}`}
              elem={menuElem}
              linkComponent={linkComponent}
            />
          );
        })}
      </ul>
    );
  }
}

export default MenuSubElems;

import React from 'react';
import MenuSubElems from './';
const props = {
  elems: [{
    href: 'url1',
    target: '',
    text: 'some text1'
  },
  {
    href: 'url2',
    target: '',
    text: 'some text 2'
  }],
  linkComponent: jest.fn()
};

describe('<MenuSubElems />', () => {

  it('should render MenuSubElems', () => {
    const wrapper = shallow(<MenuSubElems {...props} />);
    expect(wrapper).toHaveLength(1);
  });

  it('should return null if array is not provided', () => {
    const wrapper = shallow(<MenuSubElems />);
    expect(wrapper.type()).toBeNull();
  });

  const wrapper = shallow(<MenuSubElems {...props} />);
 
  it('should have a <ul>', () => {
    expect(wrapper.find('ul')).toHaveLength(1);
  });
  
  it('should have a 2 <MenuSubElem />', () => {
    expect(wrapper.find('MenuSubElem')).toHaveLength(2);
  });


});
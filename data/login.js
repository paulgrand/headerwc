/**
 *  Function implementation.
 * @return {object} - data
 * @param {object} newCbeUrl - whatcar url.
 */
const login = newCbeUrl => ({
  login: {
    'Hey %userFullName!': [
      {
        text: 'Logout',
        href: `${newCbeUrl}/logout`,
        target: '',
        type: 'external'
      }
    ]
  }
});

export default login;

/**
 *  Function implementation.
 * @return {object} - data
 * @param {object} newCbeUrl - whatcar url.
 */
const rightMenu = newCbeUrl => ({
  login: {
    text: 'Login',
    href: `${newCbeUrl}/enquiries`,
    target: '',
    type: 'external'
  },
  logout: {
    text: 'Logout',
    href: `${newCbeUrl}/logout`,
    target: '',
    type: 'external'
  },
  mydeals: {
    text: 'My offers',
    href: `${newCbeUrl}/enquiries`,
    target: '',
    type: 'external'
  },
  search: {
    text: '',
    href: '/searchURL',
    target: ''
  }
});

export default rightMenu;

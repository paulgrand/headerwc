/**
 *  Function implementation.
 * @return {object} - data
 * @param {object} whatCarUrl - whatcar url.
 */
const leftMenu = whatCarUrl => ({
  Reviews: {
    navLinks: [
      {
        text: 'New car reviews',
        href: `${whatCarUrl}/reviews`,
        dataCy: 'reviews'
      },
      {
        text: 'Used car reviews',
        href: `${whatCarUrl}/used-reviews`,
        dataCy: 'used-reviews'
      },
      {
        text: 'Long-term reviews',
        href: `${whatCarUrl}/tag/long-term-tests`,
        target: '',
        dataCy: 'long-term-tests'
      }
    ]
  },

  'Car deals': {
    navLinks: [
      {
        text: 'New car deals',
        href: `${whatCarUrl}/new-car-deals`,
        target: '',
        dataCy: 'new-car-deals'
      },
      {
        text: 'Car leasing',
        href: `${whatCarUrl}/car-leasing`,
        target: '',
        type: 'external',
        dataCy: 'car-leasing'
      }
    ]
  },
  'News & advice': {
    navLinks: [
      {
        text: 'Latest stories',
        href: `${whatCarUrl}/tag/latest`,
        target: '',
        dataCy: 'news-latest'
      },
      {
        text: 'Advice',
        href: `${whatCarUrl}/tag/advice`,
        dataCy: 'advice',
        target: ''
      }
    ]
  },

  'Best of': {
    navLinks: [
      {
        text: 'Top 10s',
        href: `${whatCarUrl}/tag/top-10`,
        dataCy: 'top-10',
        target: ''
      },
      {
        text: 'Car comparisons',
        href: `${whatCarUrl}/tag/new-car-group-tests`,
        dataCy: 'car-comparisons',
        target: ''
      },
      {
        text: 'New car awards',
        href: `${whatCarUrl}/awards`,
        target: '',
        type: 'external',
        dataCy: 'awards'
      },
      {
        text: 'Used car awards',
        href: `${whatCarUrl}/used-car-awards`,
        target: '',
        type: 'external',
        dataCy: 'used-car-awards'
      }
    ]
  },

  'Services & tools': {
    navLinks: [
      {
        text: 'Value my car',
        href: `${whatCarUrl}/car-valuation/`,
        target: '',
        dataCy: 'car-valuation',
        type: 'external'
      },
      {
        text: 'True MPG',
        href: `${whatCarUrl}/truempg/mpg-calculator`,
        target: '',
        dataCy: 'mpg-calculator',
        type: 'external'
      },
      {
        text: 'Car finance',
        href: `${whatCarUrl}/car-finance`,
        target: '',
        dataCy: 'car-finance',
        type: 'external'
      },
      {
        text: 'GAP insurance',
        href:
          '//www.ala.co.uk/whatcargap/?utm_source=whatcar&utm_medium=nav%20bar&utm_campaign=Gap%20insurance',
        target: '_blank',
        type: 'external',
        dataCy: 'gap-insurance'
      },
      {
        text: 'Car warranty',
        href:
          '//www.ala.co.uk/whatcarwarranty?utm_source=whatcar.com&utm_medium=footer&utm_campaign=What+Car%3F+Warranty&ala_p=WCWTY',
        target: '_blank',
        type: 'external',
        dataCy: 'car-warranty'
      }
    ]
  },

  Vans: {
    navLinks: [
      {
        text: 'Vans',
        href: `${whatCarUrl}/class/vans`,
        target: '',
        dataCy: 'vans'
      }
    ]
  },

  'Electric & hybrid': {
    navLinks: [
      {
        text: 'Electric & hybrid',
        href: `${whatCarUrl}/class/electric-cars`,
        target: '',
        dataCy: 'news-electric'
      }
    ]
  }
});

export default leftMenu;

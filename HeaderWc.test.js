import React from 'react';
import HeaderWc from './HeaderWc';
import style from './headerWc.scss';
import logoSVG from './whatcar_logo.svg';

const props = {
  iconArrowDown: 'an icon',
  iconArrowUp: 'an icon',
  iconMobileMenu: 'an icon',
  iconMyDeals: 'an icon',
  iconMobileMenuClose: 'an icon', 
  iconSearch: 'an icon',
  iconAvatar: 'an icon',
  logoSVG: logoSVG,
  style: style
};

const DOMRef0 = 'DOMRefNodeMain0';
const DOMRef1 = 'DOMRefNodeMain1';
const DOMRef2 = 'DOMRefNodeMain2';
const DOMRefLogin = 'DOMRefnodeLogin0';

describe('<HeaderWc />', () => {
  const wrapper = shallow(<HeaderWc {...props} />);

  it('should render a HeaderWc', () => {
    expect(wrapper).toHaveLength(1);
  });
  
  it('should have class header', () => {
    expect(wrapper.find('.classHeader')).toHaveLength(1);
  });

  it('should have tag header', () => {
    expect(wrapper.find('header')).toHaveLength(1);
  });

  it('should have tag nav', () => {
    expect(wrapper.find('nav')).toHaveLength(1);
  });

  describe('firstRow', () => {
    it('should have class .firstRow', () => {
      expect(wrapper.find('.classHeaderFirstRow')).toHaveLength(1);
    });

    it('should not render the IconLinkList if "isUsedExternal" is set to true', () => {
      const wrapper = shallow(<HeaderWc {...props} isUsedExternal={true} />);
      expect(wrapper.find('.classInfoIconsList')).toHaveLength(0);
    });

    describe('Logo', () => {
      const logo = wrapper.find('Logo');

      it('should have class logo', () => {
        expect(wrapper.find('.classHeaderLogo')).toHaveLength(1);
      });

      it('should have Logo component', () => {
        expect(logo).toHaveLength(1);
      });

      it('should have correct logo SVG', () => {
        expect(logo.props().logo).toEqual(logoSVG);
      });

      it('should use the hardcoded SVG path and use the <HayMarketLink> when "isUsedExternal" is true', () => {
        const wrapper = shallow(<HeaderWc {...props} isUsedExternal={true}/>);
        expect(wrapper.find('HayNavLink')).toHaveLength(1);
        expect(wrapper.find('svg').html()).toContain('path');
      });

      it('should have siteName WhatCar?', () => {
        expect(logo.props().siteName).toEqual('WhatCar?');
      });

    });

    it('should have MenuDesktop if viewport is not xs', () => {
        expect(wrapper.find('MenuDesktop')).toHaveLength(1);
    });

    it('should NOT have MenuDesktop if viewport is xs', () => {
        const wrapper = shallow(<HeaderWc {...props} viewport={ 'xs' }/>);
        expect(wrapper.find('MenuDesktop')).toHaveLength(0);
    });    
  });

  describe('secondRow', () => {
    const wrapper = shallow(<HeaderWc {...props} />);
        
    it('should have MenuMobile if viewport is xs', () => {
      const wrapper = shallow(<HeaderWc {...props} viewport={ 'xs' }/>);
      expect(wrapper.find('MenuMobile')).toHaveLength(1);
      expect(wrapper.find('InfoIconsList')).toHaveLength(0);
      expect(wrapper.find('OverlayWhenMenuIsOpen')).toHaveLength(0);
    });

    it('should have InfoIconsList if viewport is NOT xs', () => {
      expect(wrapper.find('MenuMobile')).toHaveLength(0);
      expect(wrapper.find('InfoIconsList')).toHaveLength(1);
    });

    it('should NOT have InfoIconsList if "isUsedExternal" is set to true', () => {
      const wrapper = shallow(<HeaderWc {...props} isUsedExternal={true}/>);
      expect(wrapper.find('InfoIconsList')).toHaveLength(0);
    });
  });

  describe('onClick, onHover', () => {

    it('should call componentDidMount &&  attach addEventListener to window', () => {
      const spyComponentDidMount = jest.spyOn(HeaderWc.prototype, 'componentDidMount');
      const spyOnAddEventListener = jest.spyOn(window, 'addEventListener');

      const wrapper = shallow(<HeaderWc {...props} />);
  
      expect(spyComponentDidMount).toHaveBeenCalledTimes(1);
      expect(spyOnAddEventListener).toHaveBeenCalledTimes(3);

      wrapper.unmount();
  
      spyComponentDidMount.mockReset();
      spyComponentDidMount.mockRestore();

      spyOnAddEventListener.mockReset();
      spyOnAddEventListener.mockRestore();
  
    });
  
    it('should call componentWillUnmount when unmount && remove eventListener from window', () => {
      const spyComponentWillMount = jest.spyOn(HeaderWc.prototype, 'componentWillUnmount');
      const spyOnRemoveEventListener = jest.spyOn(window, 'removeEventListener');

      const wrapper = shallow(<HeaderWc {...props} />);
  
      wrapper.unmount();

      expect(spyComponentWillMount).toHaveBeenCalledTimes(1);
      expect(spyOnRemoveEventListener).toHaveBeenCalledTimes(3);
      
      spyComponentWillMount.mockReset();
      spyComponentWillMount.mockRestore();

      spyOnRemoveEventListener.mockReset();
      spyOnRemoveEventListener.mockRestore();
    });
    
    it('should call handleOnHover when a hover event occur', () => {
      const spyOnHandleOnClick = jest.spyOn(HeaderWc.prototype, 'handleOnHover').mockImplementation();
      
      const wrapper = shallow(<HeaderWc {...props} />);

      window.dispatchEvent(new window.Event('mouseover'));
      window.dispatchEvent(new window.Event('mouseout'));

      expect(spyOnHandleOnClick).toHaveBeenCalledTimes(2);
      
      wrapper.unmount();
      spyOnHandleOnClick.mockReset();
      spyOnHandleOnClick.mockRestore();
    });

    it('should call handleOnClick when a click event occur', () => {
      const spyOnHandleOnClick = jest.spyOn(HeaderWc.prototype, 'handleOnClick').mockImplementation();
      
      const wrapper = shallow(<HeaderWc {...props} />);

      window.dispatchEvent(new window.Event('click'));

      expect(spyOnHandleOnClick).toHaveBeenCalledTimes(1);
      
      wrapper.unmount();
      spyOnHandleOnClick.mockReset();
      spyOnHandleOnClick.mockRestore();
    });

  });

  describe('handleOnHover', () => {
    
    afterAll( () => {
      wrapper.unmount();
    });

    [DOMRef0, DOMRef1, DOMRef2, DOMRefLogin].forEach( (currentRef, index) => {

      it(`should open the dropdown when you hover on the elem ${index + 1}`, () => {

        const wrapper = mount(<HeaderWc {...props} />);
        wrapper.setState({
          isUserLoggedIn: true
        });

        expect(wrapper.state().isMenuOpen).toBeFalsy();
  
        // It gets the actual DOM node of the Review menu elem. This is needed by the .contains function in the handleOnClick that requires an acutal node.
        const DOMRefNode = currentRef !== DOMRefLogin ? wrapper.instance().leftMenuRef.current[currentRef] : wrapper.instance().loginRef.current[currentRef];
  
        const newEvent = new window.EventTarget();
        newEvent.target = DOMRefNode;
  
        wrapper.instance().handleOnHover(newEvent);
  
        expect(wrapper.state().isMenuOpen).toBeTruthy();
  
        wrapper.update();
        
        // when click on a ref the menu needs to be open
        expect(wrapper.find('.classMenuDesktopOpen')).toHaveLength(1);

        if(currentRef === DOMRefLogin){
          // if the ref is the login, the refs in the leftmenu need to have classMenuDesktopMenuIsOpenButIAmNotActive
          expect(wrapper.find('.classMenuDesktopMenuIsOpenButIAmNotActive')).toHaveLength(7);  
        }
        else{
          // if the ref is one on the leftmenu, only the other 2 need to have classMenuDesktopMenuIsOpenButIAmNotActive, whilst the login's ref doesn't need to
          expect(wrapper.find('.classMenuDesktopMenuIsOpenButIAmNotActive')).toHaveLength(6);
        }
        
      });

    });

    it('should close the menu if the hover away from main menu elems', () => {

      const wrapper = mount(<HeaderWc {...props} />);

      wrapper.setState({
        isMenuOpen: true,
        activeRef: wrapper.instance().leftMenuRef.current[DOMRef0].id
      });
      // The menu is now open with an active ref
      expect(wrapper.state().isMenuOpen).toBeTruthy();

      // I use the header's ref
      const DOMRefNode = wrapper.instance().headerRef.current;

      const newEvent = new window.EventTarget();
      newEvent.target = DOMRefNode;

      // I pass the Header's ref which is not any of the main menu elems
      wrapper.instance().handleOnHover(newEvent);

      // The passed ref is not contained, therefore the menu gets closed.
      expect(wrapper.state().isMenuOpen).toBeFalsy();

    });

    it('should have loginRef null if isUserLoggedIn is false', () => {
      const wrapper = mount(<HeaderWc {...props} />);
      const loginRef = wrapper.instance().loginRef.current;
      expect(loginRef).toBeNull();
    });

    it('should have loginRef if isUserLoggedIn is true', () => {
      const wrapper = mount(<HeaderWc {...props} />);
      wrapper.setState({
        isUserLoggedIn: true
      });
      const loginRef = wrapper.instance().loginRef.current;
      expect(loginRef).not.toBeNull();
    });
    
  });

    describe('cookieCheck', () => {

      it('logged in state should be false if no cookie', () => {
        const wrapper = mount(<HeaderWc {...props} />);
        expect(wrapper.state().isUserLoggedIn).toEqual(false);
        expect(wrapper.state().userFullName).toEqual('');
        expect(wrapper.state().userInitial).toEqual('');
      });

      it('should change state if there is a cookie', () => {
        document.cookie="userInfo=BigHank";
        const wrapper = mount(<HeaderWc {...props} />);
        expect(wrapper.state().isUserLoggedIn).toEqual(true);
        expect(wrapper.state().userFullName).toEqual('BigHank');
        expect(wrapper.state().userInitial).toEqual('B');
      });
    });
});
// @flow

import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import data from './data/leftMenu';
import Logo from '../../Presentational/Logo';
import HayNavLink from '../../Presentational/HayNavLink';
import MenuDesktop from './subComponents/MenuDesktop';
import MenuMobile from './subComponents/MenuMobile';
import InfoIconsList from './subComponents/InfoIconsList';
import { logoSVGExternal } from './data/whatCarLogoSVG';

/**
 * <Header /> component
 * @returns {object} -
 */
export class HeaderWc extends PureComponent<*, *> {
  handleOnClick: Function;
  handleOnHover: Function;
  headerRef: Object;
  leftMenuRef: Object;
  loginRef: Object;
  /**
   * Implements propTypes.
   */
  static propTypes = {
    linkComponent: PropTypes.func,
    viewport: PropTypes.oneOf(['xs', 'sm', 'md', 'lg']).isRequired,
    iconArrowDown: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
      .isRequired,
    iconArrowUp: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
      .isRequired,
    iconMobileMenu: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
      .isRequired,
    iconMobileMenuClose: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object
    ]).isRequired,
    iconMyDeals: PropTypes.string,
    iconAvatar: PropTypes.string,
    iconSearch: PropTypes.string,
    logoSVG: PropTypes.string.isRequired,
    style: PropTypes.object.isRequired,
    whatCarUrl: PropTypes.string,
    newCbeUrl: PropTypes.string,
    isUsedExternal: PropTypes.bool
  };

  static defaultProps = {
    viewport: 'lg',
    linkComponent: HayNavLink,
    whatCarUrl: '',
    newCbeUrl: ''
  };

  static displayName = 'HeaderWc';

  /**
   * Constructor implementation.
   * @param {object} props -
   */
  constructor(props: Object): void {
    super(props);
    this.state = {
      userFullName: '',
      userInitial: '',
      isUserLoggedIn: false,
      isMenuOpen: false,
      activeRef: ''
    };
    this.handleOnClick = this.handleOnClick.bind(this);
    this.handleOnHover = this.handleOnHover.bind(this);
    this.headerRef = React.createRef();
    this.leftMenuRef = React.createRef();
    this.loginRef = React.createRef();
  }

  /**
   * It adds click event listener.
   * @return {void}
   */
  componentDidMount(): void {
    var cookie = document.cookie.match(new RegExp('userInfo' + '=([^;]+)'));
    if (cookie) {
      this.setState({
        isUserLoggedIn: true,
        userFullName: cookie[1],
        userInitial: cookie[1].charAt(0)
      });
    }
    window.addEventListener('click', this.handleOnClick);
    window.addEventListener('mouseover', this.handleOnHover);
    window.addEventListener('mouseout', this.handleOnHover);
  }

  /**
   * It removes click event listener.
   * @return {void}
   */
  componentWillUnmount(): void {
    window.removeEventListener('click', this.handleOnClick);
    window.removeEventListener('mouseover', this.handleOnHover);
    window.removeEventListener('mouseout', this.handleOnHover);
  }

  /**
   * It handles the onHover event.
   * @param {object} event - event received from hover
   * @return {void}
   */
  handleOnHover(event: ?SyntheticInputEvent<HTMLElement>): void {
    if (
      event &&
      this.headerRef &&
      this.headerRef.current &&
      this.headerRef.current.contains(event.target)
    ) {
      const mergedRefs = {
        ...this.leftMenuRef.current,
        ...this.loginRef.current
      };

      // It loops through the properties of the merged obj above (mergedRefs) and it searches a DOMRef that contains
      // the event.target.

      const isRefFound = Object.keys(mergedRefs).some(elem => {
        if (elem.indexOf('DOMRef') === 0) {
          const currentRef = mergedRefs[elem];

          if (event && currentRef.contains(event.target)) {
            this.setState(prevState => {
              return {
                isMenuOpen: true,
                activeRef: currentRef.id
              };
            });
            // It skips the rest if it finds it.
            return true;
          }
        }
        // not found or not a DOMRef property.
        return false;
      });

      // the ref was not found.
      if (!isRefFound) {
        this.closeMenu();
      }
    } else {
      // if event is null or the click come from outside the header.
      this.closeMenu();
    }
  }

  /**
   * It handles the onClick event.
   * @param {object} event - event received from the click
   * @return {void}
   */
  handleOnClick(event: ?SyntheticInputEvent<HTMLElement>): void {
    /* istanbul ignore else */
    if (event) {
      this.closeMenu();
    }
  }

  /**
   * It closes the menu directly.
   * @returns {void}
   */
  closeMenu = (): void => {
    if (this.state.isMenuOpen || this.state.activeRef !== '') {
      this.setState(prevState => {
        return {
          isMenuOpen: false,
          activeRef: ''
        };
      });
    }
  };

  /**
   * Renders header.
   *
   * @return {JSX} rendered header.
   */
  render() {
    const {
      linkComponent,
      viewport,
      iconArrowDown,
      iconArrowUp,
      iconMobileMenu,
      iconMyDeals,
      iconMobileMenuClose,
      iconSearch,
      iconAvatar,
      logoSVG,
      style,
      whatCarUrl,
      newCbeUrl,
      isUsedExternal
    } = this.props;

    const {
      classHeader,
      classHeaderFirstRow,
      classHeaderLogo,
      ...restOfStyle
    } = style;

    const isMobile = viewport === 'xs';

    const {
      isMenuOpen,
      activeRef,
      userFullName,
      userInitial,
      isUserLoggedIn
    } = this.state;

    const menuData = data(whatCarUrl);

    let logoElement = (
      <Logo
        logo={logoSVG}
        siteName={'WhatCar?'}
        linkComponent={linkComponent}
        to={whatCarUrl}
      />
    );

    // If the heading is being used in an external partnership page then the logo svg needs to
    // be a hardcoded svg, instead of a reference to the /static/media/ svg file using 'xlink:href'
    if (isUsedExternal) {
      logoElement = (
        <HayNavLink to={'https://www.whatcar.com'}>
          {logoSVGExternal}
        </HayNavLink>
      );
    }

    // Desktop menu
    const menuDesktop = (
      <MenuDesktop
        style={restOfStyle}
        data={menuData}
        iconArrowDown={iconArrowDown}
        iconArrowUp={iconArrowUp}
        linkComponent={linkComponent}
        ref={this.leftMenuRef}
        isMenuOpen={isMenuOpen}
        activeRef={activeRef}
        menuDesktopType={'leftMenu'}
      />
    );

    // Mobile menu
    const menuMobile = (
      <MenuMobile
        style={restOfStyle}
        data={menuData}
        iconArrowDown={iconArrowDown}
        iconArrowUp={iconArrowUp}
        iconMobileMenu={iconMobileMenu}
        iconMobileMenuClose={iconMobileMenuClose}
        iconClosed={iconArrowDown}
        linkComponent={linkComponent}
        userFullName={userFullName}
        isUserLoggedIn={isUserLoggedIn}
        newCbeUrl={newCbeUrl}
      />
    );

    // Info Icons List (i.e. 'My Offers' and 'Accounts' buttons)
    let showInfoIconsList = '';

    // If the heading is being used in a partnership page then don't render the infoIconList
    if (!isUsedExternal) {
      showInfoIconsList = (
        <InfoIconsList
          style={restOfStyle}
          extendStyle={style.infoIconsListWrapper}
          iconAvatar={iconAvatar}
          iconMyDeals={iconMyDeals}
          linkComponent={linkComponent}
          iconArrowDown={iconArrowDown}
          iconSearch={iconSearch}
          iconArrowUp={iconArrowUp}
          iconLoginClose={iconMobileMenuClose}
          loginRef={this.loginRef}
          isMenuOpen={isMenuOpen}
          activeRef={activeRef}
          userFullName={userFullName}
          userInitial={userInitial}
          isUserLoggedIn={isUserLoggedIn}
          newCbeUrl={newCbeUrl}
        />
      );
    }

    return (
      <Fragment>
        <header className={classHeader} ref={this.headerRef}>
          <nav data-cy="main-nav">
            <div className={classHeaderFirstRow}>
              <div className={classHeaderLogo}>{logoElement}</div>
              {!isMobile && menuDesktop}
            </div>
            <ci-search></ci-search>
            <div>{isMobile ? menuMobile : showInfoIconsList}</div>
          </nav>
        </header>
      </Fragment>
    );
  }
}

export default HeaderWc;
